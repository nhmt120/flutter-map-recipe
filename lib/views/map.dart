import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MyMap extends StatefulWidget {
  const MyMap({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MyMapState();
  }
}

class _MyMapState extends State<MyMap> {
  late LatLng userPosition;
  List<Marker> markers = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Home Screen'),
          // actions: [
          //   IconButton(
          //     icon: const Icon(Icons.map),
          //     onPressed: () => findPlaces(),
          //   )
          // ],
        ),
        body: FutureBuilder(
          future: findUserLocation(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return GoogleMap(
              initialCameraPosition: CameraPosition(
                target: snapshot.data, //LatLng(10.7572, 106.6505),
                zoom: 12,
              ),
              markers: Set<Marker>.of(markers),
            );
          },
        ));
  }
  // Find and mark restaurants within 1000m vicinity
  // Future findPlaces() async {
  //   const String key = 'AIzaSyDHChfO4JkoTmK0DPGisKApJ94EZD6TEpk';
  //   const String placesUrl =
  //       'https://maps.googleapis.com/maps/api/place/nearbysearch/json?';
  //
  //   String url = placesUrl +
  //       'key=$key&type=restaurant&location=${userPosition.latitude},${userPosition.longitude}' +
  //       '&radius=1000';
  //
  //   final response = await http.get(Uri.parse(url));
  //   if (response.statusCode == 200) {
  //     final data = json.decode(response.body);
  //     showMarkers(data);
  //   } else {
  //     throw Exception('Unable to retrieve places');
  //   }
  // }
  //
  // showMarkers(data) {
  //   List places = data['results'];
  //   markers.clear();
  //   for (var place in places) {
  //     markers.add(
  //       Marker(
  //         markerId: MarkerId(place['reference']),
  //         position: LatLng(place['geometry']['location']['lat'],
  //             place['geometry']['location']['lng']),
  //         infoWindow: InfoWindow(
  //           title: place['name'],
  //           snippet: place['vicinity'],
  //         ),
  //       ),
  //     );
  //   }
  //   setState(() {
  //     markers = markers;
  //   });
  // }

  Future<LatLng> findUserLocation() async {
    Location location = Location();
    LocationData userLocation;
    PermissionStatus hasPermission = await location.hasPermission();
    bool active = await location.serviceEnabled();
    if (hasPermission == PermissionStatus.granted && active) {
      userLocation = await location.getLocation();
      userPosition = LatLng(userLocation.latitude!, userLocation.longitude!);
    } else {
      userPosition = const LatLng(10.7572, 106.6505);
    }

    if (markers.isEmpty) {
      markers.add(buildMaker(userPosition));
    } else {
      markers[0] = buildMaker(userPosition);
    }

    setState(() {});

    return userPosition;
  }

  Marker buildMaker(LatLng pos) {
    MarkerId markerId = const MarkerId('H');
    Marker marker = Marker(markerId: markerId, position: pos);
    return marker;
  }
}
