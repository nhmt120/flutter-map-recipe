import 'package:flutter/material.dart';

import 'map.dart';

class MyMapApp extends StatelessWidget {
  const MyMapApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Map Recipe',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyMap(),
    );
  }
}